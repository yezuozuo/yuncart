<?php

/**
 * uc_server 同步通知
 */
error_reporting(0);

define('UC_CLIENT_VERSION', '1.6.0');
define('UC_CLIENT_RELEASE', '20110501');

define('API_DELETEUSER', 1);
define('API_RENAMEUSER', 1);
define('API_GETTAG', 1);
define('API_SYNLOGIN', 1);
define('API_SYNLOGOUT', 1);
define('API_UPDATEPW', 1);
define('API_UPDATEBADWORDS', 1);
define('API_UPDATEHOSTS', 1);
define('API_UPDATEAPPS', 1);
define('API_UPDATECLIENT', 1);
define('API_UPDATECREDIT', 1);
define('API_GETCREDIT', 1);
define('API_GETCREDITSETTINGS', 1);
define('API_UPDATECREDITSETTINGS', 1);
define('API_ADDFEED', 1);
define('API_RETURN_SUCCEED', '1');
define('API_RETURN_FAILED', '-1');
define('API_RETURN_FORBIDDEN', '1');

define('IN_API', true);
define('CURSCRIPT', 'api');


if (!defined('IN_UC')) {
    define("IN_CART",		true);
    define("SITEPATH", dirname(dirname(__FILE__)));

    require SITEPATH."/init.php";

    require DATADIR . '/config/ucenter.inc.php';

    $get = $post = array();

    $code = @$_GET['code'];
    parse_str(uc_authcode($code, 'DECODE', UC_KEY), $get);

    if (time() - $get['time'] > 3600) {
        exit('Authracation has expiried');
    }
    if (empty($get)) {
        exit('Invalid Request');
    }

    include_once THIRDPATH . '/uc_client/lib/xml.class.php';
    $post = xml_unserialize(file_get_contents('php://input'));

    if (in_array($get['action'], array('test', 'deleteuser', 'renameuser', 'gettag', 'synlogin', 'synlogout', 'updatepw', 'updatebadwords', 'updatehosts', 'updateapps', 'updateclient', 'updatecredit', 'getcredit', 'getcreditsettings', 'updatecreditsettings', 'addfeed'))) {
        $uc_note = new uc_note();
        echo $uc_note->$get['action']($get, $post);
        exit();
    } else {
        exit(API_RETURN_FAILED);
    }
} else {
    exit;
}

class uc_note {

    var $dbconfig = '';
    var $db = '';
    var $tablepre = '';
    var $appdir = '';

    function _serialize($arr, $htmlon = 0) {
        if (!function_exists('xml_serialize')) {
            include_once DISCUZ_ROOT . './uc_client/lib/xml.class.php';
        }
        return xml_serialize($arr, $htmlon);
    }

    function uc_note() {
        
    }

    function test($get, $post) {
        return API_RETURN_SUCCEED;
    }

    function deleteuser($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function renameuser($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function gettag($get, $post) {
        global $_G;
        if (!API_GETTAG) {
            return API_RETURN_FORBIDDEN;
        }
        return $this->_serialize(array($get['id'], array()), 1);
    }

    function synlogin($get, $post) {
        global $_G;

        if (!API_SYNLOGIN) {
            return API_RETURN_FORBIDDEN;
        }

        header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');

        $uid = intval($get['uid']);
        //可能要补全本地用户信息
        $user = DB::getDB()->selectrow("user", "uid,uname,pass,salt,lastpost", "uid='" . $uid . "'");
        if (empty($user)) {
            $uc_user = uc_get_user($uid, true);//为了取email
            if (!$uc_user) {
                return API_RETURN_FAILED;
            }
            $data = array('uid' => $uid, "uname" => $uc_user[1], "email" => $uc_user[2]);
            $data += encpass();
            $data["regip"] = getClientIp();
            $data["lasttime"] = $data["regtime"] = time();
            $uid = DB::getDB()->insert("user", $data);
            if ($uid < 1) {
                return API_RETURN_FAILED;
            }
        }
        $_SESSION["uname"] = $uname;
        $_SESSION["uid"] = $user["uid"];
        $_SESSION['lastpost'] = $user['lastpost'];
        DB::getDB()->update("user", "lasttime=" . time(), "uid='{$uid}'");
        return API_RETURN_SUCCEED;
    }

    function synlogout($get, $post) {

        if (!API_SYNLOGOUT) {
            return API_RETURN_FORBIDDEN;
        }

        header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');

        if ($_SESSION["uid"]) {
            //判断用户的类型
            $uid = intval($_SESSION['uid']);
            $source = DB::getDB()->selectval("user", "source", "uid='$uid'");
            if ($source)
                unset($_SESSION['oauth']);
            unset($_SESSION['uid'], $_SESSION['uname']);
        }
    }

    function updatepw($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function updatebadwords($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function updatehosts($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function updateapps($get, $post) {
        global $_G;

        if (!API_UPDATEAPPS) {
            return API_RETURN_FORBIDDEN;
        }

        $UC_API = '';
        if ($post['UC_API']) {
            $UC_API = str_replace(array('\'', '"', '\\', "\0", "\n", "\r"), '', $post['UC_API']);
            unset($post['UC_API']);
        }

        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/apps.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'apps\'] = ' . var_export($post, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        if ($UC_API && is_writeable(DISCUZ_ROOT . './config/config_ucenter.php')) {
            if (preg_match('/^https?:\/\//is', $UC_API)) {
                $configfile = trim(file_get_contents(DISCUZ_ROOT . './config/config_ucenter.php'));
                $configfile = substr($configfile, -2) == '?>' ? substr($configfile, 0, -2) : $configfile;
                $configfile = preg_replace("/define\('UC_API',\s*'.*?'\);/i", "define('UC_API', '" . addslashes($UC_API) . "');", $configfile);
                if ($fp = @fopen(DISCUZ_ROOT . './config/config_ucenter.php', 'w')) {
                    @fwrite($fp, trim($configfile));
                    @fclose($fp);
                }
            }
        }
        return API_RETURN_SUCCEED;
    }

    function updateclient($get, $post) {
        global $_G;

        if (!API_UPDATECLIENT) {
            return API_RETURN_FORBIDDEN;
        }

        $cachefile = DISCUZ_ROOT . './uc_client/data/cache/settings.php';
        $fp = fopen($cachefile, 'w');
        $s = "<?php\r\n";
        $s .= '$_CACHE[\'settings\'] = ' . var_export($post, TRUE) . ";\r\n";
        fwrite($fp, $s);
        fclose($fp);

        return API_RETURN_SUCCEED;
    }

    function updatecredit($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function getcredit($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function getcreditsettings($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function updatecreditsettings($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

    function addfeed($get, $post) {
        return API_RETURN_FORBIDDEN;
    }

}
